package fr.dyade.koala.test;

import java.io.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.net.*;

import junit.framework.Assert;
import junit.framework.TestCase;
import fr.dyade.koala.xml.koml.*;

import org.xml.sax.*;

import fr.dyade.koala.serialization.GeneratorOutputStream;
import fr.dyade.koala.serialization.ObjectOutputHandler;

/**
 * @version $Revision$
 * @author  Philippe Le H�garet
 */
public class Test extends TestCase {

	
	public void testSimpleKoml() throws Exception {
		String filePath = "target/instances.xml"; 
		File file = new File(filePath);
		FileOutputStream fos = new FileOutputStream(file); 
		Object[] objects = new Object[] {new Date(), new URL("http://www.inria.fr/koala/XML/"), new Button("Open"), new Vector(), new PInteger(), new PInteger2()};
		KOMLSerializer ob = new KOMLSerializer(fos, false);
		for(Object object : objects) {
			ob.addObject(object);
		}
		ob.close();
		fos.close();
		List<Object> read = new ArrayList<Object>();
		FileInputStream fis = new FileInputStream(file); 
		KOMLDeserializer koml = new KOMLDeserializer(fis, false);
		boolean readAll = false;
		try {
		    while (true) {
		    	read.add(koml.readObject());
		    }
		} 
		catch (EOFException e) {
			readAll = true;
		} 
		finally {
		    koml.close();
		    fis.close();
		}
		Assert.assertTrue("Should throw EOFException at the end of the file", readAll);
		Assert.assertEquals("Should read same number of objects as written", objects.length, read.size());
		int index = 0;
		for(Object object : objects) {
			Assert.assertEquals("Should read the same type of object", object.getClass(), read.get(index).getClass());
			index++;
		}
	}
	
	
    /**
     * 
     */
    public static void serialize() throws Exception {
	KOMLSerializer ob = new KOMLSerializer("instances.xml", false);
	ob.addObject(new Date());
	ob.addObject(new URL("http://www.inria.fr/koala/XML/"));
	ob.addObject(new Button("Open"));
	ob.addObject(new Vector());
	ob.addObject(new PInteger());
	ob.addObject(new PInteger2());

	ob.close();
    }    
    
    /**
     * 
     */
    public static void deserialize() 
	    throws IOException, ClassNotFoundException {
	KOMLDeserializer koml = new KOMLDeserializer("instances.xml", false);


	try {
	    while (true) {
		System.err.println( koml.readObject() );
	    }
	} catch (EOFException e) {
	} finally {
	    koml.close();
	}
    }

    /**
     * 
     */
    public static void main(String[] args) throws Exception {
	try {
	    if (args.length == 0) {
		serialize();
	    } else if (args.length == 1) {
		deserialize();
	    } 
	} catch (SAXException e) {
	    if (e.getException() != null) {
		e.getException().printStackTrace();
	    } else {
		e.printStackTrace();
	    }
	}
	System.exit( 0 );
    }
}


class PInteger implements Serializable {
    int i = 6;
}

